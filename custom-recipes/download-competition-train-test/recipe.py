import dataiku
import typing
import logging

import kaggledss

kaggledss.utils.activate_logging('debug')


def _get_output_datasets_dict(*role_name: str) -> typing.Dict[str, dataiku.Dataset]:
    result = {}
    for role in role_name:
        dataset = kaggledss.utils.io_recipes.unary_output_role(role_name, 'dataset', strict=False)
        if dataset:
            result[role] = dataset
    return result


_output_datasets = _get_output_datasets_dict('train_dataset', 'test_dataset', 'submission_example_dataset')
_extra_folder = kaggledss.utils.io_recipes.unary_output_role('additional_files_folder', 'dataset', strict=False)

logging.debug('Output datasets are % and folder %s', _output_datasets, _extra_folder)

params_dict = customrecipe.get_recipe_config()

competition = kaggledss.KaggleCompetition(
    **kaggledss.utils.pop_from_signature(params_dict, kaggledss.KaggleCompetition)
)


logging.critical('Raising Assertion error to stop execution')

assert 0

# additional_folder_name = customrecipe.get_output_names_for_role(role_name)
#
# params_dict = customrecipe.get_recipe_config()
#
# kaggledss.get_kaggle_data_object(**params_dict).upload_files_to_dir(kaggle_download_dir)
#
#
# # Write recipe outputs
# train_dataset = dataiku.Dataset("titanic-train")
# train_dataset.write_with_schema(train_df)
# test_dataset = dataiku.Dataset("titanic-test")
# test_dataset.write_with_schema(titanic_test_df)
# submission_dataset = dataiku.Dataset("titanic-submission")
# submission_dataset.write_with_schema(titanic_submission_df)
