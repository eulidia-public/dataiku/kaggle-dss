from dataiku import customrecipe

import kaggledss

kaggledss.activate_logging('debug')

file_dataset = kaggledss.utils.io_recipes.unary_output_role('output_dataset', 'dataset', strict=True)

params_dict = customrecipe.get_recipe_config()

files_df = kaggledss.get_kaggle_data_object(**params_dict).get_files()

file_dataset.write_with_schema(files_df)
