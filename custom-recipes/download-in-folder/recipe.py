import dataiku
from dataiku import customrecipe

import kaggledss

kaggledss.activate_logging('debug')

folder_name, = customrecipe.get_output_names_for_role('output_folder')
kaggle_download_dir = dataiku.Folder(folder_name)

params_dict = customrecipe.get_recipe_config()
kaggledss.get_kaggle_data_object(**params_dict).upload_files_to_dir(kaggle_download_dir)
