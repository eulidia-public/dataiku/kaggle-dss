# -*- coding: utf-8 -*-
import os
import zipfile
import tempfile
import subprocess
import contextlib
import sysconfig
import logging
import pathlib
import typing
import abc
import enum
import io
import re

import numpy as np
import pandas as pd


_ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\\/]*[@-~]')

logger = logging.getLogger(__name__)


@contextlib.contextmanager
def _inside_temp_dir():
    cwd = os.getcwd()
    with tempfile.TemporaryDirectory() as tmpdirname:
        logger.debug('created temporary directory %s', tmpdirname)
        try:
            os.chdir(tmpdirname)
            yield pathlib.Path(tmpdirname)
        finally:
            os.chdir(cwd)
    logger.debug('Deleted %s', tmpdirname)


class KaggleDataObjects(enum.Enum):
    competitions = 'competitions'
    datasets = 'datasets'


class KaggleData(abc.ABC):
    @staticmethod
    @abc.abstractmethod
    def type_of_data() -> KaggleDataObjects:
        """competitions or datasets"""
        pass

    def __init__(self, username: str, token: str, kaggle_object_name: str):
        self._username = username
        self._token = token
        self._name = kaggle_object_name
        logger.debug('Created kaggle object %r', self)

    def __str__(self):
        return f'{self.type_of_data()}: {self._name}'

    def __repr__(self):
        return f'{self.__class__.__name__}({self._username}, {self._name})'

    def _kaggle_env_vars(self) -> typing.Dict[str, str]:
        return dict(
            KAGGLE_USERNAME=self._username,
            KAGGLE_KEY=self._token
        )

    def _execute_kaggle_cli(self, *args) -> subprocess.CompletedProcess:
        # Add the Kaggle args to the subprocess env vars
        env = os.environ.copy()
        env.update(self._kaggle_env_vars())

        command_parts = [os.path.join(sysconfig.get_paths()['scripts'], 'kaggle'), self.type_of_data().value]
        command_parts.extend(args)

        completed_proc = subprocess.run(
            command_parts,
            env=env,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )

        logger.debug('STDOUT is %s', completed_proc.stdout)
        logger.debug('STDERR is %s', completed_proc.stderr)

        try:
            completed_proc.check_returncode()
        except subprocess.CalledProcessError as err:
            err_output = err.output or completed_proc.stderr

            err_output = err_output.decode('utf-8')

            if "403" in err_output:
                err_msg = "You have no access to the competition/dataset. Have you agreed to the terms and conditions?"
            elif "404" in err_output:
                err_msg = f"The {self.type_of_data()} {self._name} does not exist"
            else:
                err_msg = f"Unknown error from Kaggle CLI ({err_output})"

            logger.critical('Raising error %s from %s', err_msg, err_output)
            raise ValueError(err_msg) from err

        return completed_proc

    def _download_files(self) -> typing.List[str]:
        self._execute_kaggle_cli('download', '-c', self._name)

        zip_filename, = pathlib.Path('.').glob('*.zip')
        logger.debug('Downloaded %s', zip_filename)

        with zipfile.ZipFile(zip_filename, 'r') as zip_ref:
            file_list = zip_ref.namelist()
            logger.debug('Found compressed files %s', file_list)
            zip_ref.extractall()

        os.remove(zip_filename)

        logger.info("Downloaded and extracted %s", file_list)

        return file_list

    def upload_files_to_dir(self, upload_to_folder) -> None:
        """
        Recovers the files from the Kaggle API and uploads them to a DSS managed folder
        :param upload_to_folder: dataiku.Folder to upload
        :return: None
        """
        with _inside_temp_dir() as tempdirpath:
            file_list = self._download_files()

            for filename in file_list:
                filepath = tempdirpath / filename
                logging.debug('Uploading %s', filepath)
                with filepath.open('rb') as fp:
                    upload_to_folder.upload_stream(filepath.name, fp)

            logging.info('Uploaded all files to target directory')

    def get_files(self) -> pd.DataFrame:
        completed_proc = self._execute_kaggle_cli('files', '--csv', self._name)
        buffer = io.StringIO(_ansi_escape.sub('', completed_proc.stdout.decode('utf-8')))
        df = pd.read_csv(buffer, skip_blank_lines=True)
        logging.debug('Recovered files DF %s', df)
        return df


class KaggleCompetition(KaggleData):
    @staticmethod
    def type_of_data() -> KaggleDataObjects:
        return KaggleDataObjects.competitions

    def infer_train_test_submission(
            self,
            train: bool = True,
            test: bool = True,
            submission: bool = True
    ) -> typing.Dict[str, str]:
        func_args = locals().copy()
        func_args.pop('self')
        names_to_match = [name for name, switch in func_args.items() if switch]
        files_to_match = self.get_files()['name']
        print(names_to_match, files_to_match)

        pd.DataFrame([np.nan])



class KaggleDataset(KaggleData):
    @staticmethod
    def type_of_data() -> KaggleDataObjects:
        return KaggleDataObjects.datasets


def get_kaggle_data_object(
        username: str,
        token: str,
        data_object: typing.Union[KaggleDataObjects, str],
        kaggle_object_name: str
) -> KaggleData:
    if isinstance(data_object, str):
        try:
            data_object = getattr(KaggleDataObjects, data_object)
        except AttributeError:
            # noinspection PyTypeChecker
            raise ValueError(
                f'Invalid data_object {data_object} '
                f'(Expected {list(map(lambda i: i.value, KaggleDataObjects))})'
            )

    if data_object == KaggleDataObjects.competitions:
        return KaggleCompetition(username=username, token=token, kaggle_object_name=kaggle_object_name)
    elif data_object == KaggleDataObjects.datasets:
        return KaggleDataset(username=username, token=token, kaggle_object_name=kaggle_object_name)
    else:
        raise ValueError(f'Invalid dataset_or_competition {data_object}')
