import logging
import typing

import dataiku
from dataiku import customrecipe


logger = logging.getLogger(__name__)

DatasetFolder = typing.Union[dataiku.Folder, dataiku.Dataset]


def get_unary_role(
    role_name: str,
    cls: typing.Union[type, str],
    in_out: typing.Union[typing.Callable, str],
    strict: bool = False,
) -> DatasetFolder:
    if not isinstance(cls, type):
        if cls.lower() == "dataset":
            cls = dataiku.Dataset
        elif cls.lower() == "folder":
            cls = dataiku.Folder
        else:
            raise ValueError(
                f'Invalid cls argument {cls} (Expected "dataset" or "folder")'
            )

    elif cls is not dataiku.Dataset and cls is not dataiku.Folder:
        raise ValueError(f'Invalid cls argument {cls} (Expected "dataset" or "folder")')

    if not isinstance(in_out, typing.Callable):
        if in_out.lower() in ("in", "input"):
            in_out = customrecipe.get_input_names_for_role
        elif in_out.lower() in ("out", "output"):
            in_out = customrecipe.get_input_names_for_role
        else:
            raise ValueError(
                f'Invalid in_out argument {in_out} (Expected "in" or "out")'
            )

    logger.debug("Getting role %s %s with %s", cls.__name__, role_name, in_out.__name__)
    dataset_name = in_out(role_name)
    logger.debug("Role %s is %s", role_name, dataset_name)

    if dataset_name:
        (dataset_name,) = dataset_name
        return cls(dataset_name)

    if strict:
        raise ValueError(f"Role {role_name} is empty")

    logging.info("Role %s is empty", role_name)
    return None


def get_unary_output_role(
    role_name: str, cls: typing.Union[type, str] = dataiku.Dataset, strict: bool = False
) -> DatasetFolder:
    return get_unary_role(
        role_name, cls, customrecipe.get_output_names_for_role, strict
    )


def get_unary_input_role(
    role_name: str, cls: typing.Union[type, str] = dataiku.Dataset, strict: bool = False
) -> DatasetFolder:
    return get_unary_role(role_name, cls, customrecipe.get_input_names_for_role, strict)
