import inspect
import typing


def pop_dict_from_signature(config_dict: dict, func: typing.Callable) -> dict:
    return {arg_name: config_dict.pop(arg_name) for arg_name in inspect.signature(func).parameters}
