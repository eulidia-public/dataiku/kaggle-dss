# -*- coding: utf-8 -*-
import logging
from .core import KaggleCompetition, KaggleDataset, KaggleDataObjects, get_kaggle_data_object
from . import utils

logger = logging.getLogger(__name__)


def activate_logging(level: str = 'info'):
    logger.setLevel(getattr(logging, level.upper()))
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('[%(asctime)s][%(name)s][%(levelname)s]: %(message)s'))
    logger.addHandler(handler)


__all__ = [
    'KaggleDataObjects', 'KaggleCompetition', 'KaggleDataset', 'get_kaggle_data_object',
    'utils', 'activate_logging'
]
